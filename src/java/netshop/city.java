/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package netshop;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public class city extends HttpServlet {
// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       doPost(request,response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       String data = "{\"count\":17,\"list\":[{\"pcaID\":\"1\",\"pcaname\":\"市\"},{\"pcaID\":\"0101\",\"pcaname\":\"安庆市\"},{\"pcaID\":\"0102\",\"pcaname\":\"蚌埠市\"},{\"pcaID\":\"0103\",\"pcaname\":\"亳州市\"},{\"pcaID\":\"0104\",\"pcaname\":\"巢湖市\"},{\"pcaID\":\"0105\",\"pcaname\":\"池州市\"},{\"pcaID\":\"0106\",\"pcaname\":\"滁州市\"},{\"pcaID\":\"0107\",\"pcaname\":\"阜阳市\"},{\"pcaID\":\"0108\",\"pcaname\":\"合肥市\"},{\"pcaID\":\"0109\",\"pcaname\":\"淮北市\"},{\"pcaID\":\"0110\",\"pcaname\":\"淮南市\"},{\"pcaID\":\"0111\",\"pcaname\":\"黄山市\"},{\"pcaID\":\"0112\",\"pcaname\":\"六安市\"},{\"pcaID\":\"0113\",\"pcaname\":\"马鞍山市\"},{\"pcaID\":\"0114\",\"pcaname\":\"宿州市\"},{\"pcaID\":\"0115\",\"pcaname\":\"铜陵市\"},{\"pcaID\":\"0116\",\"pcaname\":\"芜湖市\"},{\"pcaID\":\"0117\",\"pcaname\":\"宣城市\"}]}";
		response.setContentType("text/html;charset=gb2312");
response.setHeader("Access-Control-Allow-Origin", "*");
            try (PrintWriter out = response.getWriter()) {
                out.println(data);
                out.flush();
            }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
