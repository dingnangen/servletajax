/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public class scoreinfo extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int item = Integer.parseInt(request.getParameter("i"));
        if (item > 15) {
            item = 15;
        }
        int i = 0;
        String data1 = "{\"point\":\"与客户预约不成功未再次预约或告知水掌柜，造成延误\",\"create_date\":\"2013-12-09\",\"shiXiang\":\"与客户预约不成功未再次预约或告知水掌柜，造成延误\",\"beizhu\":\"zjh：uuiiooppp\",\"yhcpwhID\":\"xql\"}";
        String data2 = "{\"point\":\"被客户特别表扬\",\"create_date\":\"2013-12-09\",\"shiXiang\":\"被客户特别表扬\",\"beizhu\":\"zjh：null\",\"yhcpwhID\":\"小路\"}";

        String data = "";
        for (i = 0; i < item; i++) {
            if (i == 0) {
                data = data1;
            } else {
                if (i % 2 != 0) {
                    System.out.println(i % 2);
                    data = data + ',' + data2;
                } else {
                    System.out.println(i % 2);
                    data = data + ',' + data1;
                }
            }
        }
        data = '[' + data + ']';
        response.setContentType("text/html;charset=gb2312");
        response.setHeader("Access-Control-Allow-Origin", "*");
        try (PrintWriter out = response.getWriter()) {
            out.println(data);
            out.flush();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
