/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public class daihuifang extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int item = Integer.parseInt(request.getParameter("i"));
        if (item > 15) {
            item = 15;
        }
        int i = 0;
        String data1 = "{\"models\":\"橱下RO机( )\",\"imgs\":\"YYD1378439214127_1.jpg,YYD1378439214127_2.jpg,YYD1378439214127_3.jpg,YYD1378439214127_4.jpg\",\"username\":\"清幽\",\"yuyuedanID\":\"YYD1378439214127\",\"address\":\"刺桐路东方银座\",\"wcshijian\":\"2013-12-26\",\"tel\":\"13960475978\",\"xiaoshoudanId\":\"XSD13781163248121\"}";
        String data2 = "{\"models\":\"橱下超滤机(MU131-5)\",\"imgs\":\"YYD1387337599765_095106.png\",\"username\":\"王传宏\",\"yuyuedanID\":\"YYD1387337599765\",\"address\":\"安徽省 芜湖市 繁昌县 新港镇 （富鑫钢铁）\",\"wcshijian\":\"2013-12-19\",\"tel\":\"13855338333\",\"xiaoshoudanId\":\"XSD1383374529467\"}";
        String data = "";
        for (i = 0; i < item; i++) {
            if (i == 0) {
                data = data1;
            } else {
                if (i % 2 != 0) {
                    System.out.println(i % 2);
                    data = data + ',' + data2;
                } else {
                    System.out.println(i % 2);
                    data = data + ',' + data1;
                }
            }
        }
        data = '[' + data + ']';
        response.setContentType("text/html;charset=gb2312");
        response.setHeader("Access-Control-Allow-Origin", "*");
        try (PrintWriter out = response.getWriter()) {
            out.println(data);
            out.flush();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
