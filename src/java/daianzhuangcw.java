/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public class daianzhuangcw extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int i = Integer.parseInt(request.getParameter("i"));

        String data1 = "{\"count\":1,\"list\":[{\"anzhuangdizi\":\"泉州市经济开发区虹桥路108号\",\"kehuming\":\"洪七\",\"xsdID\":\"BXSD13946063520571\",\"yuyueshijian\":\"2014-04-18\",\"jixing\":\"储水型电热水器\",\"pinpaiming\":\"美的-佛山\",\"pgzhuangtai\":\"客户自己解决\",\"wcshijian\":\"2014-04-18\",\"kehuID\":\"13506099999\",\"pgdID\":\"PGD1394606367698\"}]}";
        String data2 = "{\"count\":1,\"list\":[{\"anzhuangdizi\":\"春晖家园38栋804室\",\"kehuming\":\"秦琴\",\"xsdID\":\"BXSD13851061992035\",\"yuyueshijian\":\"2014-04-23\",\"jixing\":\"即热型电热水器\",\"pinpaiming\":\"海尔\",\"pgzhuangtai\":\"客户自己解决\",\"wcshijian\":\"2014-04-23\",\"kehuID\":\"13855557577\",\"pgdID\":\"PGD1397871665842\"}]}";

        String data = "";
        if (i>5) {
           
            data = "{\"count\":0,\"list\":[]}";
        } else if (i % 2 != 0) {
            System.out.println(i % 2);
            data = data1;
        } else {
            System.out.println(i % 2);
            data = data2;
        }

        response.setContentType("text/html;charset=gb2312");
        response.setHeader("Access-Control-Allow-Origin", "*");
        try (PrintWriter out = response.getWriter()) {
            out.println(data);
            out.flush();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
