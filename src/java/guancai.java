/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public class guancai extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int i = Integer.parseInt(request.getParameter("i"));

        String data1 = "{\"count\":6,\"list\":[{\"danjia\":2,\"danwei\":\"个\",\"mingcheng\":\"90度弯头\",\"guige\":\"20\"},{\"danjia\":30,\"danwei\":\"个\",\"mingcheng\":\"铜弯头活接\",\"guige\":\"\"},{\"danjia\":2,\"danwei\":\"个\",\"mingcheng\":\"等径直接头\",\"guige\":\"20\"},{\"danjia\":4,\"danwei\":\"个\",\"mingcheng\":\"等径三通\",\"guige\":\"20\"},{\"danjia\":3,\"danwei\":\"个\",\"mingcheng\":\"45度弯头\",\"guige\":\"20\"},{\"danjia\":22,\"danwei\":\"个\",\"mingcheng\":\"外丝弯头\",\"guige\":\"20*1/2\"}]}";
        String data2 = "{\"count\":6,\"list\":[{\"danjia\":22,\"danwei\":\"个\",\"mingcheng\":\"内丝三通\",\"guige\":\"20*1/2*20\"},{\"danjia\":21,\"danwei\":\"个\",\"mingcheng\":\"靠墙内丝弯头\",\"guige\":\"20*1/2\"},{\"danjia\":22,\"danwei\":\"个\",\"mingcheng\":\"加长弯活接\",\"guige\":\"20\"},{\"danjia\":20,\"danwei\":\"个\",\"mingcheng\":\"活接三通\",\"guige\":\"20\"},{\"danjia\":6,\"danwei\":\"个\",\"mingcheng\":\"过桥弯管\",\"guige\":\"20\"},{\"danjia\":1.5,\"danwei\":\"个\",\"mingcheng\":\"塑料管卡\",\"guige\":\"20\"}]}";
        String data = "";
        if (i > 9) {

            data = "{\"count\":0,\"list\":[]}";
        } else if (i % 2 != 0) {
            System.out.println(i % 2);
            data = data1;
        } else {
            System.out.println(i % 2);
            data = data2;
        }

        response.setContentType("text/html;charset=gb2312");
        response.setHeader("Access-Control-Allow-Origin", "*");
        try (PrintWriter out = response.getWriter()) {
            out.println(data);
            out.flush();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
