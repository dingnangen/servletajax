/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public class installationInfo extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        
        String data = "{\"biaoti\":\"RO纯水机维护保养小常识\",\"neirong\":\"◎</SPAN> </FONT></SPAN>高压泵不启动，无法造水</SPAN> </br>" +
"</FONT></SPAN>①</SPAN> </FONT></SPAN>检查是否停电，插头是否插上</SPAN> </br>" +
"</FONT></SPAN>②</SPAN> </FONT></SPAN>检查低压开关是否失灵，不能接通电源</SPAN> </br>" +
"</FONT></SPAN>③</SPAN> </FONT></SPAN>检查水泵和变压器是否短路，或整机线路连接有误</SPAN> </br>" +
"</FONT></SPAN>④</SPAN> </FONT></SPAN>检查高压开关或水位控制器（指立式冰热机）是否失灵，无法复位</SPAN> </br>" +
"</FONT></SPAN>⑤</SPAN> </FONT></SPAN>检查电脑盒是否有故障（指微电脑型）</SPAN> </FONT></br>" +
"</SPAN></SPAN>◎</SPAN> </FONT></SPAN>高压泵正常工作，但无法造水</SPAN> </br>" +
"</FONT></SPAN>①</SPAN> </FONT></SPAN>高压泵失压</SPAN> </br>" +
"</FONT></SPAN>②</SPAN> </FONT></SPAN>进水电磁阀打不开无法进水（纯水浓水均无）</SPAN> </br>" +
"</FONT></SPAN>③</SPAN> </FONT></SPAN>前置滤芯堵塞（纯水浓水均无或浓水很小）</SPAN> </br>" +
"</FONT></SPAN>④</SPAN> </FONT></SPAN>逆止阀失灵（有废水无纯水）</SPAN> </br>" +
"</FONT></SPAN>⑤</SPAN> </FONT></SPAN>自动冲洗电磁阀失灵，不能有效关闭（一直处于冲洗状态）</SPAN> </br>" +
"</FONT></SPAN>⑥</SPAN> </FONT></SPAN>电脑盒有故障不能关闭反冲电磁阀（一直处于冲洗状态）</SPAN> </br>" +
"</FONT></SPAN>⑦</SPAN>RO </FONT></SPAN>膜堵塞</SPAN> </FONT></br>" +
"</SPAN></SPAN>◎</SPAN> </FONT></SPAN>高压泵不停机</SPAN> </br>" +
"</FONT></SPAN>①</SPAN> </FONT></SPAN>高压泵压力不足，不能达到高压设定的压力</SPAN> </br>" +
"</FONT></SPAN>②</SPAN> </FONT></SPAN>逆止阀堵塞，不出纯水</SPAN> </br>" +
"</FONT></SPAN>③</SPAN> </FONT></SPAN>高压开关失灵，无法起跳</SPAN> </FONT></br>" +
"</SPAN></SPAN>◎</SPAN> </FONT></SPAN>高压泵停机，但废水不停</SPAN> </br>" +
"</FONT></SPAN>①</SPAN> </FONT></SPAN>进水电磁阀失灵，不能有效断水</SPAN> </br>" +
"</FONT></SPAN>②</SPAN> </FONT></SPAN>电脑盒有故障，不能关闭进水电磁阀（指微电脑型）</SPAN> </br>" +
"</FONT></SPAN>③</SPAN> </FONT></SPAN>逆止阀泄压（浓水流量小）</SPAN> </FONT></br>" +
"</SPAN></SPAN>◎</SPAN> </FONT></SPAN>水满后，机器反复起跳</SPAN> </br>" +
"</FONT></SPAN>①</SPAN> </FONT></SPAN>逆止阀泄压</SPAN> </br>" +
"</FONT></SPAN>②</SPAN> </FONT></SPAN>高压开关或液位开关（指立式冰热机）失灵</SPAN> </br>" +
"</FONT></SPAN>③</SPAN> </FONT></SPAN>纯水管路系统有泄压现象</SPAN> </FONT></br>" +
"</SPAN></SPAN>◎</SPAN> </FONT></SPAN>压力桶水满，但纯水无法流出</SPAN> </br>" +
"</FONT></SPAN>①</SPAN> </FONT></SPAN>压力桶泄压，充气至桶内水刚好排出为止，约</SPAN> 0.3MPa </FONT></SPAN>，按气压表补压至</SPAN>0.04-0.08MPa </FONT><\\/SPAN>。</SPAN>\"}";
        response.setContentType("text/html;charset=gb2312");
        response.setHeader("Access-Control-Allow-Origin", "*");
        try (PrintWriter out = response.getWriter()) {
            out.println(data);
            out.flush();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
