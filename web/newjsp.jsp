<%-- 
    Document   : newjsp
    Created on : 2014-3-14, 14:02:46
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type="text/javascript">
	var xmlHttpRequest = null; //声明一个空的对象以接受XMLHttpRequest对象
	function ajaxRequest() {
		if(window.ActiveXObject) {   			//IE的
			xmlHttpRequest = new ActionXObject("Microsoft.XMLHTTP");
		}
		else if(window.XMLHttpRequest) {		//除IE外的
			xmlHttpRequest = new XMLHttpRequest();
		}
		if(xmlHttpRequest != null) {
			xmlHttpRequest.open("GET", "AjaxServlet", true);
			//关联好ajax的回调函数
			xmlHttpRequest.onreadystatechange = ajaxCall;
			
			//真正向服务器发送请求
			xmlHttpRequest.send();
		}
	} 
	function ajaxCall() {
		if(xmlHttpRequest.readyState == 4 ) {  		//完全得到服务器的响应
			if(xmlHttpRequest.status == 200) {		//没有异常
				var text = xmlHttpRequest.responseText;
				document.getElementById("div1").innerHTML = text;
			}
		}
	}
</script>
    </head>
    <body>
        <h1>Hello World!</h1>
        <button onclick="ajaxRequest()">request</button>
        <div id="div1"></div>
    </body>
</html>
